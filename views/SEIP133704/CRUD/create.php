<?php
include_once ('../../../vendor/autoload.php');
use App\SEIP133704\CRUD\Student;
use App\SEIP133704\CRUD\Utility;
use App\SEIP133704\CRUD\Message;

?>
<!DOCTYPE html>
<html lang="eng">
<head>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.js"></script>

</head>
<title>Student</title>
<body>
<div class="container">
    <h2>Student Create</h2>
    <a href="index.php"><button class="btn btn-success">Home</button></a>
    <a href="index.php"><button class="btn btn-success">Create</button></a>
    <h3>Enter Info</h3>
    <form class="form-group" action="store.php" method="post">
        <input name="id" placeholder="ID" >
        <input name="firstname" placeholder="First Name" >
        <input name="middlename" placeholder="Middle Name" >
        <input name="lastname" placeholder="Last Name" >
        <button class="btn btn-success">Submit</button>
    </form>
</div>
</body>
</html>


