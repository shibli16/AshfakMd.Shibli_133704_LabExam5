<?php
include_once ('../../../vendor/autoload.php');
use App\SEIP133704\CRUD\Student;
use App\SEIP133704\CRUD\Utility;
use App\SEIP133704\CRUD\Message;


$newUpdate = new Student();
$newUpdate->prepare($_POST);
$newUpdate->update();