<?php
include_once ('../../../vendor/autoload.php');
use App\SEIP133704\CRUD\Student;
use App\SEIP133704\CRUD\Utility;
use App\SEIP133704\CRUD\Message;

$newDelete = new Student();
$newDelete->prepare($_GET);
$newDelete->delete();