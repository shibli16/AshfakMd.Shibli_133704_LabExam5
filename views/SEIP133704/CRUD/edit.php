<?php
include_once ('../../../vendor/autoload.php');
use App\SEIP133704\CRUD\Student;
use App\SEIP133704\CRUD\Utility;
use App\SEIP133704\CRUD\Message;
 $newEdit = new Student();
    $newEdit->prepare($_GET);
    $item = $newEdit->view();
?>
<!DOCTYPE html>
<html lang="eng">
<head>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.js"></script>

</head>
<title>Student</title>
<body>
<div class="container">
    <h2>Student Update</h2>
    <a href="index.php"><button class="btn btn-success">Home</button></a>
    <a href="index.php"><button class="btn btn-success">Create</button></a>
    <h3>Edit Info</h3>
    <h3>Your ID <?php echo $item->id ?></h3>
    <h4>If you have wrong ID, going back to home delete your entry and Create again your data.</h4>
    <form class="form-group" action="update.php" method="post">
        <input name="id" placeholder="ID" value="<?php echo $item->id ?>" hidden>
        <input name="firstname" placeholder="First Name" value="<?php echo $item->firstname ?>" >
        <input name="middlename" placeholder="Middle Name" value="<?php echo $item->middlename ?>" >
        <input name="lastname" placeholder="Last Name" value="<?php echo $item->lastname ?>">
        <button class="btn btn-success">Submit</button>
    </form>
</div>
</body>
</html>


