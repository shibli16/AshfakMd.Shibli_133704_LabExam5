<?php
include_once ('../../../vendor/autoload.php');
use App\SEIP133704\CRUD\Student;
use App\SEIP133704\CRUD\Utility;
use App\SEIP133704\CRUD\Message;
session_start();

$newIndex = new Student();
$list = $newIndex->index();



?>
<!DOCTYPE html>
<html lang="eng">
    <head>
        <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
        <script src="../../../resource/bootstrap/js/bootstrap.js"></script>

    </head>
<title>Student</title>
<body>
    <div class="container">
        <h2>Student Index</h2>
            <a href="index.php"><button class="btn btn-success">Home</button></a>
        <a href="create.php"><button class="btn btn-success">Create</button></a>
        <h2><?php echo Message::message()?></h2>
        <table class="table table-responsive table-bordered">
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>Action</th>
            </tr>
            <?php
            $sl = 0;
            foreach ($list as $item){
                
            $sl++;
            ?>
            <tr>
                <td><?php echo $sl ?></td>
                <td><?php echo $item->id ?></td>
                <td><?php echo $item->firstname ?></td>
                <td><?php echo $item->middlename ?></td>
                <td><?php echo $item->lastname ?></td>
                <td>
                    <a href="view.php?id=<?php echo $item->id ?>"><button class="btn btn-success">View</button></a>
                    <a href="edit.php?id=<?php echo $item->id ?>"><button class="btn btn-warning">Edit</button></a>
                    <a href="delete.php?id=<?php echo $item->id ?>"><button class="btn btn-danger">Delete</button></a>
                    
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
</body>
</html>


