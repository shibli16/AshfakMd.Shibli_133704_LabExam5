<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 6/19/2016
 * Time: 2:50 PM
 */

namespace App\SEIP133704\CRUD;
if(!isset($_SESSION['message'])){
    session_start();
}

class Message
{
    public static function message($message = NULL){
        if(is_null($message)){
            $_message = self::getMessage();
            return $_message;
        }
        else self::setMessage($message);
    }
    public function getMessage(){
        $_message = $_SESSION['message'];
        $_SESSION['message']= "";
        return $_message;
    }
    public function setMessage($message){
        $_SESSION['message'] = $message;
    }

}