<?php
namespace App\SEIP133704\CRUD;

/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 6/19/2016
 * Time: 2:40 PM
 */
class Student
{
    public $id = "";
    public $firstName= "";
    public $middleName= "";
    public $lastName= "";
    public $conn;

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost","root","","labxm5b22") or die("error in connection");
    }
    public function prepare($data = array()){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];

        }
        if(array_key_exists('firstname',$data)){
            $this->firstName = $data['firstname'];

        }
        if(array_key_exists('middlename',$data)){
            $this->middleName= $data['middlename'];

        }
        if(array_key_exists('lastname',$data)){
            $this->lastName = $data['lastname'];

        }

    }
    public function store(){
        $query = "INSERT INTO `labxm5b22`.`student` (`id`, `firstname`, `middlename`, `lastname`) VALUES ('".$this->id."', '".$this->firstName."', '".$this->middleName."', '".$this->lastName."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Utility::redirect("index.php");
            Message::message("Data inserted Successfully");
        }
        else Message::message("Unsuccessful Insert");
    }
    public  function index(){
        $_list = array();
        $query= "SELECT * FROM `student`";
        $result = mysqli_query($this->conn,$query);
        while ($row = mysqli_fetch_object($result)){
            $_list [] = $row;
        }
        return $_list;

    }
    public function view(){

        $query= "SELECT * FROM `student` WHERE `id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_object($result);
        return $row;

}
    public function update(){

        $query1 = "UPDATE `labxm5b22`.`student` SET `firstname` = '".$this->firstName."', `middlename` = '".$this->middleName."', `lastname` = '".$this->lastName."' WHERE `student`.`id` =".$this->id;

            $result1 = mysqli_query($this->conn,$query1);

            if($result1) {
                Utility::redirect("index.php");
                Message::message("Data updated Successfully ");
            }
            else Message::message("Unsuccessful Edit. You May Have entered a duplicate ID. please Delete your Entry And Add New with new ID");



    }
    public function delete(){
        $query= "DELETE FROM `labxm5b22`.`student` WHERE `student`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Utility::redirect("index.php");
            Message::message("Data Deleted Successfully");
        }
        else Message::message("Delete unsuccessful");
    }


}